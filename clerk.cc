#include <iostream>
using namespace std;

#include <TH1.h>
#include <TH2.h>
#include <TFile.h>

#if ROOT_VER == 6
#include <TTreeReader.h>
#else
#include <TTree.h>
#endif

#include <TPRegexp.h>
#include <TSystemDirectory.h>

const short N_BARS = 8;
//charge
const short CHARGE_LIMIT = 1000;

TPRegexp time_re("(\\d{4})(\\d{2})(\\d{2})-(\\d{2})(\\d{2})(\\d{2})");
TPRegexp mode_re("ext.*?(direct|coinc)?(?!.*(direct|coinc).*)");
TPRegexp hv_re("HV([0-9]+V)");
TPRegexp trLVL_re("([0-9]+mV)");

vector<string> formMatchV(TObjArray const* matches)
{
    vector<string> res;
    for (TObject const* t : *matches)   res.push_back( t->GetName() );
    delete matches;
    return res;
}

TString parse(char const* fname, bool usePHolders=true)
{
	return fname;
	
    char const* mode = mode_re.Match(fname) ? "ext" : usePHolders ? "~~~" : "";
    vector<string> toks = formMatchV( mode_re.MatchS(fname) );
    string subMode = toks.size()==2 ? toks[1] : usePHolders ? "~~~~~~" : "";
    string hv = hv_re.Match(fname) ? formMatchV(hv_re.MatchS(fname))[1] : usePHolders ? "~~~" : "";
    string trLvl = trLVL_re.Match(fname) ? formMatchV(trLVL_re.MatchS(fname))[1] : usePHolders ? "~~~~" : "";

    return TString::Format("%-3s.%6s HV: %3s, trLVL: %4s", mode, subMode.c_str(), hv.c_str(), trLvl.c_str());
}


struct Bar {
    TH1* ch1_ampH;
    TH1* ch2_ampH;
    TH2* ampH2;
    TH1* mean_ampH;
    //charge
    TH1* ch1_chargeH;
    TH1* ch2_chargeH;
    TH1* mean_chargeH;

public:
    Bar(TFile* rf, short bi)
    {
        TString run_info = parse(rf->GetTitle(), false);
        ch1_ampH = new TH1F(Form("ch%d_amp", bi), TString::Format("%s Ch: %d", run_info.Data(), bi), 100, 0, 600);
        ch2_ampH = new TH1F(Form("ch%d_amp", bi+N_BARS), TString::Format("%s Ch: %d", run_info.Data(), bi+N_BARS), 100, 0, 600);
        ampH2 = new TH2F("ampH2", TString::Format("%s Bar: %d", run_info.Data(), bi+1), 100, 0, 600, 100, 0, 600);
        mean_ampH = new TH1F("mean_amp", TString::Format("%s Bar: %d", run_info.Data(), bi+1), 100, 0, 600);
		//charge
		ch1_chargeH = new TH1F(Form("ch%d_charge", bi), TString::Format("%s Ch: %d", run_info.Data(), bi), 100, 0, CHARGE_LIMIT);
    	ch2_chargeH = new TH1F(Form("ch%d_charge", bi+N_BARS), TString::Format("%s Ch: %d", run_info.Data(), bi+N_BARS), 100, 0, CHARGE_LIMIT);
		mean_chargeH = new TH1F("mean_charge", TString::Format("%s Bar: %d", run_info.Data(), bi+1), 100, 0, CHARGE_LIMIT);
     
    
    #if ROOT_VER == 6
        TTreeReader tr("adc64", rf);
        TTreeReaderValue<short> ch1_amp(tr, TString::Format("ch%d.amp", bi));
        TTreeReaderValue<short> ch2_amp(tr, TString::Format("ch%d.amp", bi+N_BARS));
		//charge
		TTreeReaderValue<short> ch1_charge(tr, TString::Format("ch%d.charge", bi));
        TTreeReaderValue<short> ch2_charge(tr, TString::Format("ch%d.charge", bi+N_BARS));
    #else 
        short amps[2];
        short *ch1_amp=amps, *ch2_amp=amps+1;
		short charges[2], *ch1_charge=charges, *ch2_charge=charges+1;
        TTree* tr = (TTree*) rf->Get("adc64");
        tr->SetBranchAddress(TString::Format("ch%d.amp", bi), ch1_amp);
        tr->SetBranchAddress(TString::Format("ch%d.amp", bi+N_BARS), ch2_amp);
        //charge
        tr->SetBranchAddress(TString::Format("ch%d.charge", bi), ch1_charge);
        tr->SetBranchAddress(TString::Format("ch%d.charge", bi+N_BARS), ch2_charge);
    #endif

    #if ROOT_VER == 6    
        while (tr.Next())
    #else 
        for (ulong e=0; e<tr->GetEntries(); e++)
    #endif
        {
            ch1_ampH->Fill(*ch1_amp);
            ch2_ampH->Fill(*ch2_amp);
            ampH2->Fill(*ch1_amp, *ch2_amp);
            mean_ampH->Fill( (*ch1_amp + *ch2_amp) / 2.0 );
			//charge
			ch1_chargeH->Fill(*ch1_charge);
			ch2_chargeH->Fill(*ch2_charge);
			mean_chargeH->Fill( (*ch1_charge + *ch2_charge) / 2.0 );
        } 
    }

    void write() const
    {
        ch1_ampH->Write();
        ch2_ampH->Write();
        ampH2->Write();
        mean_ampH->Write();
        //charge
        ch1_chargeH->Write();
        ch2_chargeH->Write();
        mean_chargeH->Write();
    }
};

struct Run {
    static short id;

    Bar* bars[N_BARS];
    TString dirname;
    TFile* rf;
    TNamed root_file = TNamed("root_file", "");

public:
    Run(char const* _dirname, char const* fname) : dirname(_dirname), rf(TFile::Open(fname))
    {
        for (short bi=0; bi<N_BARS; bi++) bars[bi] = new Bar(rf, bi);
        root_file.SetTitle(fname);
    }
    ~Run() { rf->Close(); }

    void write() const
    {
        // gDirectory->mkdir(TString::Format("%d", id++))->cd();
        gDirectory->mkdir(dirname)->cd();
        for (short bi=0; bi<N_BARS; bi++)
        {
            gDirectory->mkdir(TString::Format("bar%d", bi+1))->cd();
            bars[bi]->write();
            gDirectory->cd("..");
        }
        root_file.Write();
        gDirectory->cd("..");
    }
};
short Run::id = 0;

// map<TDatime, Run*> run_map;
map<TString, map<TDatime, Run*>> cosmo_runs, sr90_runs, other;

TDatime getTime(char const* fname)
{
    vector<string> toks = formMatchV( time_re.MatchS(fname) );
    return toks.empty() ? TDatime() : TDatime(TString::Format( "%s-%s-%s %s:%s:%s",
        toks[1].c_str(),
        toks[2].c_str(),
        toks[3].c_str(),
        toks[4].c_str(),
        toks[5].c_str(),
        toks[6].c_str() ));
}


void clerk(char const* dir_path)
{
    ushort f_counter = 0;
    for (TObject* o : *TSystemDirectory("", dir_path).GetListOfFiles())
    {
        TString fname = o->GetTitle();
        fname += '/';
        fname += o->GetName();
        TDatime dt = getTime(o->GetName());
        if (fname.EndsWith(".root")) {
            if (fname.SubString("cosmo").Length()) 
                cosmo_runs[parse(o->GetName())][dt] = new Run(dt.AsSQLString(), fname.Data());
            else if (fname.SubString("Sr90").Length())
                sr90_runs[parse(o->GetName())][dt] = new Run(dt.AsSQLString(), fname.Data());
            else
                other[parse(o->GetName())][dt] = new Run(dt.AsSQLString(), fname.Data());
            // if (++f_counter == 10) break;
        }
    }

    TFile rf("runs_base.root", "recreate");
    
    gDirectory->mkdir("Cosmics")->cd();
    for (auto p : cosmo_runs) 
    {
        gDirectory->mkdir(p.first.Data())->cd();
        for (auto p2 : p.second) 
        {
            p2.second->write();
            delete p2.second;
        }
        gDirectory->cd("..");
    }
    gDirectory->cd("..");
    
    gDirectory->mkdir("Sr90")->cd();
    for (auto p : sr90_runs) 
    {
        gDirectory->mkdir(p.first.Data())->cd();
        for (auto p2 : p.second) 
        {
            p2.second->write();
            delete p2.second;
        }
        gDirectory->cd("..");
    }
    gDirectory->cd("..");
    
    gDirectory->mkdir("Other")->cd();
    for (auto p : other) 
    {
        gDirectory->mkdir(p.first.Data())->cd();
        for (auto p2 : p.second) 
        {
            p2.second->write();
            delete p2.second;
        }
        gDirectory->cd("..");
    }
    gDirectory->cd("..");
    
    rf.Close();
}

int main(int argc, char** argv)
{
    clerk(argv[1]);
    return 0;
}
