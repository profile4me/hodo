#include "Slider.h"
#include <TApplication.h>
#include <TFile.h>
#include <TTree.h>
#include <TGraph.h>
#include <TGButton.h>
#include <TRootEmbeddedCanvas.h>
using namespace std;

const short N_SAMPLES = 51;





/*
TTreeReader* tr;
TTreeReaderArray<short>* samples;
TTreeReaderArray<short>* peak;
TTreeReaderValue<short>* zl;
TTreeReaderValue<short>* amp;
*/
TTree* tr;
short samples[N_SAMPLES];
short peak[3];
short zl, amp;

TGraph wf, peakMarkers, zl_marker;
TH1S* ampH;
TH1S* maskH;
TH1S* ampH_picked;
Slider* sl;
long curEv = 0;

TH1* testH;

void init()
{
	/*
    tr = new TTreeReader("adc64", TFile::Open("hodo.root"));
    samples = new TTreeReaderArray<short>(*tr, "ch0.samples");
    peak = new TTreeReaderArray<short>(*tr, "ch0.peak");
    zl = new TTreeReaderValue<short>(*tr, "ch0.zl");
    amp = new TTreeReaderValue<short>(*tr, "ch0.amp");
    */
    tr = (TTree*)TFile::Open("hodo.root")->Get("adc64"); 
	cout << "----------- adc64 --------------\n";
	tr->Print();
	cout << "--------------------------------\n";
    tr->Branch("ch0.samples", samples);
	tr->Branch("ch0.peak", peak);
	tr->Branch("ch0.zl", &zl);
	tr->Branch("ch0.amp", &amp);
	
    peakMarkers.SetMarkerStyle(21);
    zl_marker.SetLineColor(2);
    ampH = new TH1S("amp", "", 50, 0, 600);
    maskH = new TH1S("mask", "", 50, 0, 600);
    ampH_picked = new TH1S("amp_picked", "", 50, 0, 600);
}

void fillAmpH()
{
    //while (tr->Next()) ampH->Fill(**amp);
    for (long e=0; e<tr->GetEntries(); e++) tr->GetEntry(e);
    //tr->Restart();

    setPad("ampPad");
    ampH->Draw();
    updatePad("ampPad");
}

void drawWF()
{
    short si;
    for (si=0; si<N_SAMPLES; si++) wf.SetPoint(si, si, samples[si]);
    for (short i=0; i<3; i++) 
    {
        si = peak[i];
        peakMarkers.SetPoint(i, si, samples[si]);
    }
    zl_marker.SetPoint(0, 0, zl);
    zl_marker.SetPoint(1, N_SAMPLES, zl);

    setPad("wfPad");
    wf.Draw();
    peakMarkers.Draw("same,p");
    zl_marker.Draw("same");
    updatePad("wfPad");    
}

void switchEntry(short step=1)
{
    float min, max;
    sl->GetPosition(&min, &max);
    do
    {
		//long e = tr->GetCurrentEntry() + step;
        //if (e < 0 || e >= tr->GetEntries()) return;
        if (curEv+step < 0 || curEv+step >= tr->GetEntries()) return;
        else curEv += step;
        //tr->SetEntry(e);
        tr->GetEntry(curEv);
    } while (amp < min || max < amp);
    
    drawWF();
    cout << "Entry: " << /*tr->GetCurrentEntry()*/curEv << endl;
}

void viewer()
{
    init();
    
    TGMainFrame* w = new TGMainFrame(gClient->GetRoot());
    TRootEmbeddedCanvas* pad;

    pad = new TRootEmbeddedCanvas("wfPad", w, 600, 300);    
    gDirectory->Add(pad->GetCanvas());
    w->AddFrame(pad, new TGLayoutHints(kLHintsExpandX|kLHintsExpandY));
    
    pad = new TRootEmbeddedCanvas("ampPad", w, 600, 300);
    gDirectory->Add(pad->GetCanvas());
    w->AddFrame(pad, new TGLayoutHints(kLHintsExpandX|kLHintsExpandY));
    fillAmpH();

    sl = new Slider(w);
    setPad("ampPad");
    float l_margin = gPad->GetLeftMargin() * gPad->GetWw();
    float r_margin = gPad->GetRightMargin() * gPad->GetWw();
    w->AddFrame(sl, new TGLayoutHints(kLHintsExpandX, l_margin, r_margin));

    TGHorizontalFrame* butPad = new TGHorizontalFrame(w);
    butPad->AddFrame(new TGTextButton(butPad, "prev", "switchEntry(-1)")); 
    butPad->AddFrame(new TGTextButton(butPad, "next", "switchEntry()")); 
    w->AddFrame(butPad, new TGLayoutHints(kLHintsCenterX));

    w->MapSubwindows();
    w->Resize(w->GetDefaultSize());
    w->MapWindow();
}

int main(int argc, char** argv)
{
	TApplication app("viewer", &argc, argv);
	viewer();
	app.Run();
	return 0;
}
