#include <TGFrame.h>
#include "FileBrowser.h"

void initGui() {
    TGMainFrame* w = new TGMainFrame(gClient->GetRoot(), 400, 400);

    w->AddFrame(new FileBrowser(w), new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));

    w->MapSubwindows();
    w->SetSize(w->GetDefaultSize());
    w->MapWindow(); 
}

int main(int argc, char* args[]) {
    TApplication app("guiTest", &argc, args);
    initGui();
    app.Run();
    return 0;
}
