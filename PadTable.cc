#include "PadTable.h"
//#include "FileBrowser.h"
#include <TGDNDManager.h>
#include <TBufferFile.h>
#include <TKey.h>
#include <TCanvas.h>
#include <TSystem.h>
#include <TBrowser.h>
#include <TGFileBrowser.h>
#include <TApplication.h>
#include <iostream>
using namespace std;

ClassImp(PadTable)
ClassImp(MainFrame)

bool EmbeddedCanvas::HandleDNDDrop(TDNDData *data)
{
    TBufferFile buf(TBuffer::kRead, data->fDataLength, data->fData);
    TObject* obj = buf.ReadObject(TObject::Class());
    if (obj->InheritsFrom("TKey")) obj = ((TKey*)obj)->ReadObj();
    
    cout << obj->ClassName() << endl;
    
    if (!obj->InheritsFrom("TH1")) return true;
    TH1* h = (TH1*) obj;
    h->GetXaxis()->SetRangeUser(60, 600);
    
    if (h->GetDimension()==1)
    {
        double totW = h->Integral(h->GetXaxis()->GetFirst(), h->GetXaxis()->GetLast()); 
        h->Scale(1 / totW);
    
        h->SetLineColor( prevH ? prevH->GetLineColor()+1 : 1 );
        h->Draw( prevH ? "same hist" : "hist" );

        if (leg) delete leg; 
        leg = gPad->BuildLegend();
        prevH = h;
    }
    else 
    {
        //h->GetYaxis()->SetRangeUser(60, 600);
        h->Draw("colz");
        prevH = 0;
    }

    gPad->Update();
    return true;
}

PadTable::PadTable(TGWindow const* p) : TGCompositeFrame(p), 
	run_dir(/*gSystem->GetWorkingDirectory()*/"/home/profile4me/dborisenko/hodo")
{
    for (ushort r=0; r<N_ROWS; r++)
        for (ushort c=0; c<N_COLS; c++)
            pads[r][c] = nullptr;        

    grid = new TGCompositeFrame(this);
    grid->SetLayoutManager(new TGTableLayout(grid, 10, 10));
    fillPad();    
    AddFrame(grid, new TGLayoutHints(kLHintsExpandX|kLHintsExpandY));    

    TGCompositeFrame* control = new TGHorizontalFrame(this);
    sp1 = new TGNumberEntry(control, 1, 2, 0, TGNumberFormat::kNESInteger, 
        TGNumberFormat::kNEAPositive, TGNumberFormat::kNELLimitMinMax, 0, 10);
    control->AddFrame(sp1);
    sp2 = new TGNumberEntry(control, 1, 2, 0, TGNumberFormat::kNESInteger,
        TGNumberFormat::kNEAPositive, TGNumberFormat::kNELLimitMinMax, 0, 10);
    control->AddFrame(sp2);
    saveAs_fname = new TGTextEntry(control, "pics/tmp.png");
    saveAs_fname->SetWidth(100);
    control->AddFrame(saveAs_fname);
    TGTextButton* saveBtn = new TGTextButton(control, "Save");
    control->AddFrame(saveBtn);     
    AddFrame(control, new TGLayoutHints(kLHintsCenterX));

    Connect(sp1, "ValueSet(Long_t)", "PadTable", this, "update()");
    Connect(sp2, "ValueSet(Long_t)", "PadTable", this, "update()");
    Connect(saveBtn, "Clicked()", "PadTable", this, "saveGrid()");
}

void PadTable::fillPad(ushort r, ushort c)
{
    ushort w = (c>0) ? pads[r][c-1]->GetWidth() : 4;
    ushort h = (r>0) ? pads[r-1][c]->GetHeight() : 4;
    pads[r][c] = new EmbeddedCanvas(grid, w, h);
    grid->AddFrame(pads[r][c], new TGTableLayoutHints(c,c+1,r,r+1, PAD_HINTS));
}

void PadTable::clearPad(ushort r, ushort c)
{
    grid->RemoveFrame(pads[r][c]);
    pads[r][c]->MoveResize(-1,-1,1,1);
    delete pads[r][c]; 
    pads[r][c] = nullptr;
}

void PadTable::update()
{
    ushort rows = (ushort) sp1->GetNumber();
    ushort cols = (ushort) sp2->GetNumber();
    for (ushort r=0; r<N_ROWS; r++)
    {
        for (ushort c=0; c<N_COLS; c++)
        {
            if (r < rows && c < cols)
            {
                if (!pads[r][c])    fillPad(r, c);
            }
            else 
            {
                if (pads[r][c])     clearPad(r, c);
            }
        }
    }
    grid->MapSubwindows();
    grid->Layout();
}

void PadTable::saveGrid()
{
    TString fname = run_dir + '/' + saveAs_fname->GetText(); 
    cout << "Saving as: " << fname.Data() << endl;
    grid->SaveAs(fname);
}

MainFrame::MainFrame() {	MainFrame(600, 400);	}
MainFrame::MainFrame(int w, int h) : TGMainFrame(gClient->GetRoot(), w, h)
{
    SetLayoutManager(new TGHorizontalLayout(this));

    TGFileBrowser* br_view = new TGFileBrowser(this, nullptr, 150);
    TBrowser* br = new TBrowser("", "", br_view);
    br_view->SetBrowser(br);
    AddFrame(br_view/*new FileBrowser(this)*/, new TGLayoutHints(kLHintsExpandY));

    AddFrame(new PadTable(this), new TGLayoutHints(kLHintsExpandX|kLHintsExpandY));

    MapSubwindows();
    Layout();
    MapWindow();

    Connect("CloseWindow()", "MainFrame", this, "terminate()");
}
MainFrame::~MainFrame() {}

void MainFrame::terminate()
{
    gApplication->Terminate(0);
}
