COMPILER = g++ --std=c++11 #-g
ROOT_VER = $(shell root-config --version | sed -E 's/([0-9]).*/\1/')
COMPAT_OPTIONS = -D ROOT_VER=$(ROOT_VER)

RAW_FILES = $(wildcard $(input)/*.txt)
CONVERTED_FILES = $(patsubst $(input)/%.txt,$(input)/converted/%.root,$(RAW_FILES))

.PHONY: run all show_vars clean

build/viewer: viewer.cc
	$(COMPILER) `root-config --cflags --glibs` -o $@ $^

build/test: test.cc build/FileBrowserDict.cxx
	$(COMPILER) `root-config --cflags --glibs` -I. -o $@ $^

run: $(CONVERTED_FILES)

$(input)/converted/%.root: $(input)/%.txt
	./build/conv $< $@ 

all: build/conv build/clerk build/grapher

build/FileBrowserDict.cxx: FileBrowser.h
	rootcint -f $@ -c -p $< 

build/conv: conv.cc
	$(COMPILER) `root-config --cflags --libs` -o $@ $^

build/clerk: clerk.cc
	$(COMPILER) `root-config --cflags --libs` $(COMPAT_OPTIONS) -o $@ $^

build/grapher: grapher.cc PadTable.cc build/PadTableDict.cxx #build/FileBrowserDict.cxx
	$(COMPILER) `root-config --cflags --glibs` -I. -o $@ $^

build/PadTableDict.cxx: PadTable.h GeneralLinkDef.h
	rootcint -f $@ -c $^

show_vars:
	@printf "COMPAT_OPTIONS: %s\n", "$(COMPAT_OPTIONS)"

clean:
	@find build -type f -exec rm {} \;
