#ifndef FILE_BROWSER_H
#define FILE_BROWSER_H

#include <TGFileBrowser.h>
#include <TBrowser.h>
#include <TGListTree.h>
#include <TBufferFile.h>
#include <iostream>
#include <algorithm>

class FileBrowser : public TGFileBrowser {

	sLTI_t selected;
	TBufferFile buf;
	
	void select(TGListTreeItem* i)
	{
		selected.push_back(i);
	}
	
	bool unselect(TGListTreeItem* i) 
	{
		sLTI_i it = std::find(std::begin(selected),
			std::end(selected),
			i);
		if (it == std::end(selected)) return false;
		selected.remove(i);
		return true;
	}
	
	void toggle(TGListTreeItem* i)
	{
		if (!unselect(i)) select(i);
	}

	void unselectAll()
	{
		selected.clear();
	}
	
	void applySelected()
	{
		for (TGListTreeItem* i : selected) i->SetActive(true);
	}
	
public:
    FileBrowser(TGWindow* w) : TGFileBrowser(w), buf(TBuffer::kWrite)
    {
        SetBrowser(new TBrowser("", "", this));

		fListTree->Connect("Clicked(TGListTreeItem*,int,UInt_t,int,int)", 
			"FileBrowser", this, "clickListener(TGListTreeItem*,int,UInt_t,int,int)");
		fListTree->Connect("ProcessedEvent(Event_t*)", 
			"FileBrowser", this, "dndListener(Event_t*)");
	}


	void clickListener(TGListTreeItem* i, int btn, UInt_t mask, int x, int y)
	{
		fListTree->ClearHighlighted();
		fListTree->UnselectAll(true);
	
		if (btn == kButton1) 
		{
			if (mask != kKeyControlMask) unselectAll();
			toggle(i);
		}
		applySelected();
	}

	void dndListener(Event_t* ev)
	{
		if (ev->fType == kLeaveNotify && gDNDManager->IsDragging())
		{
			std::cout << "start dnd\n";

			buf.Reset();
			buf.WriteObject(new TList);

			TDNDData* data = fListTree->GetDNDData(0);
			data->fData = buf.Buffer();
			data->fDataLength = buf.Length();
		}
	}

	ClassDef(FileBrowser, 0)
};

#endif
