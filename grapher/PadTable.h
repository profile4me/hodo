#ifndef PADTABLE_H
#define PADTABLE_H

#include <TRootEmbeddedCanvas.h>
#include <TH1.h>
#include <TLegend.h>
#include <TGFrame.h>
#include <TGNumberEntry.h>
#include <TGTableLayout.h>

const unsigned short N_ROWS = 10;
const unsigned short N_COLS = 10;
const int PAD_HINTS = kLHintsFillX|kLHintsFillY|kLHintsExpandX|kLHintsExpandY|kLHintsShrinkX|kLHintsShrinkY;


class EmbeddedCanvas : public TRootEmbeddedCanvas {

    TH1* prevH=0;
    TLegend* leg=0;
    short row=1;
    short col=1;

public:
    EmbeddedCanvas(TGWindow const* p, uint w=10, uint h=10) 
    : TRootEmbeddedCanvas("can", p, w, h) {}

	static TObject* getDNDObject(TDNDData*);
	virtual void DNDDrop(TObject* obj) {Emit("DNDDrop(TObject* obj)", (Long_t)obj);}	//*SIGNAL*
    bool HandleDNDDrop(TDNDData*);

	ClassDef(EmbeddedCanvas, 0)
};


class PadTable : public TGCompositeFrame {

    std::string run_dir;
    TRootEmbeddedCanvas* pads[N_ROWS][N_COLS];
    TGCompositeFrame* grid;
    TGNumberEntry* sp1;
    TGNumberEntry* sp2;
    TGTextEntry* saveAs_fname;

    void fillPad(unsigned short r=0, unsigned short c=0);
    void clearPad(unsigned short r, unsigned short c);

public:
    PadTable(TGWindow const* p);
    
    void dndDropHandler(TObject*);
    
    void update();
    void saveGrid();

	ClassDef(PadTable, 0)
};

class GrapherFrame : public TGMainFrame {
public:
	GrapherFrame(UInt_t w = 800, UInt_t h = 400);
}; 

#endif
