#ifndef LIST_TREE
#define LIST_TREE

#include <TGListTree.h>

class ListTree : public TGListTree
{
public:
	ListTree(TGCanvas* c) : TGListTree(c, kHorizontalFrame) {}

	bool HandleDNDLeave()
	{
		
		std::cout << "dnd leaves\n";
		return TGListTree::HandleDNDLeave();
	}
	
	ClassDef(ListTree, 0)
};

#endif
