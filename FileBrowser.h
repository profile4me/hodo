#ifndef FILE_BROWSER_H
#define FILE_BROWSER_H

#include <TGFileBrowser.h>
#include <TBrowser.h>
#include <TGListTree.h>
#include <iostream>
#include <algorithm>
#include <TApplication.h>

class FileBrowser : public TGFileBrowser {
    TBrowser* br;
	sLTI_t selected;
public:
    FileBrowser(TGWindow* w) : TGFileBrowser(w) {
        br = new TBrowser("", "", this);
        SetBrowser(br);
        fListTree->Disconnect("Clicked(TGFileBrowserItem*, Int_t, Int_t, Int_t)", this);
		fListTree->Connect("Clicked(TGListTreeItem*, Int_t, UInt_t, Int_t, Int_t)", 
			"FileBrowser", this, "handleItemClick(TGListTreeItem*, Int_t, UInt_t, Int_t, Int_t)");
	}
    ~FileBrowser() {
		SetBrowser(NULL);
		delete br;
	}
	
	Bool_t unselect(TGListTreeItem* i) {
		sLTI_i it = std::find(std::begin(selected), 
			std::end(selected),
			i);
		if (it != std::end(selected)) {
			//fListTree->HighlightItem(i, kFALSE, kFALSE);
			selected.remove(i);
			return kTRUE;
		}
		return kFALSE;
	}
	
	Bool_t unselect() {
		//for (TGListTreeItem* i : selected) fListTree->HighlightItem(i, kFALSE, kFALSE);
		selected.clear();
		return kTRUE;
	}
	
	void select(TGListTreeItem* i) {
		//fListTree->HighlightItem(i, kTRUE, kFALSE);
		selected.push_back(i);
	}
	
    void handleItemClick(TGListTreeItem* i, Int_t btn, UInt_t mask, Int_t x, Int_t y) {
		if (btn == kButton1) {
			if (mask != kKeyControlMask) {
				unselect();
				select(i);
			} else if (!unselect(i)) select(i);
		}
		TGFileBrowser::Clicked(i, btn, x, y);
		for (TGListTreeItem* i : selected) i->SetActive(kTRUE);
		std::cout << "Selected: " << selected.size() << '\n';
	}
	
	ClassDef(FileBrowser, 0)
};

#endif

//#ifdef __CINT__
//	#pragma link C++ class FileBrowser!;
//#endif
