#include <TH1.h>
#include <TCanvas.h>
#include <TGFrame.h>
#include <TGDoubleSlider.h>
#include <TDirectory.h>
#include <iostream>

TCanvas* getPad(char const* pname) { return (TCanvas*) gDirectory->Get(pname); }
void setPad(char const* pname) { getPad(pname)->cd(); }
void updatePad(char const* pname) { getPad(pname)->Update(); }

class Slider : public TGDoubleHSlider {
    bool changed;

    TH1* ampH;
    TH1* maskH;
    TH1* ampH_picked;
    TCanvas* ampPad;
public:
    Slider(TGWindow const* p) : TGDoubleHSlider(p), changed(false)
    {
        ampH = (TH1*)gDirectory->Get("amp");
        maskH = (TH1*)gDirectory->Get("mask");
        ampH_picked = (TH1*)gDirectory->Get("amp_picked");
        ampPad = getPad("ampPad");

        // std::cout << "ampH addr:\t\t" << ampH << std::endl;
        // std::cout << "maskH addr:\t\t" << maskH << std::endl;
        // std::cout << "ampH_picked addr:\t" << ampH_picked << std::endl;
        // std::cout << "ampPad addr:\t\t" << ampPad << std::endl;

        Connect("PositionChanged()", "Slider", this, "ChangeHandler()");
        Connect("Released()", "Slider", this, "ReleaseHandler()");
        SetRange(0, 600);
        SetPosition(0, 600);
    }

    void ChangeHandler()
    {
        changed = true;
    }
    void ReleaseHandler()
    {
        if (!changed) return;

        float min, max;
        GetPosition(&min, &max);
        
        std::cout << '[' << min << ", " << max << "]\n";
        
        short bi;
        for (bi=1; bi<maskH->FindBin(min); bi++)
            maskH->SetBinContent(bi, 0);
        for (; bi<=maskH->FindBin(max); bi++)
            maskH->SetBinContent(bi, 1);
        for (; bi<=maskH->GetNbinsX(); bi++)
            maskH->SetBinContent(bi, 0);
        ampH_picked->Multiply(ampH, maskH);
        ampPad->cd();
        ampH_picked->Draw();
        ampPad->Update();

        changed = false;
    }
	ClassDef(Slider, 0)
};

