#include <string>
#include <fstream>
#include <limits>
#include <iostream>
using namespace std;
#include <TFile.h>
#include <TTree.h>

const short N_SAMPLES = 51;
const short N_CHANNELS = 16;

struct ChEntry {
    short samples[N_SAMPLES];
    short peak[3];
    short zl;
    short amp;
    short charge;
};

TTree* adc64;
ChEntry ch_entries[N_CHANNELS];

void init()
{
    char br_name[50], leaf_list[50];
    for (short ch=0; ch<N_CHANNELS; ch++)
    {
        sprintf(br_name, "ch%d", ch);
        sprintf(leaf_list, "samples[%d]/S:peak[3]/S:zl/S:amp/S:charge/S", N_SAMPLES);
        adc64->Branch(br_name, ch_entries+ch, leaf_list);
    }
}

void conv(char const* txt_file, char const* root_file)
{
    TFile hodo(root_file, "recreate");
    adc64 = new TTree("adc64", "adc64");
    init();

    long ev=0;
    ifstream fs(txt_file);
    string s;
    short *samples, *peak, *zl, *charge, min;
    while (!fs.eof())
    {
        if (++ev % 10000 == 0) printf("Ev: %ld\n", ev);
        for (short i=0; i<3; i++) fs >> s;
        for (short ch=0; ch<N_CHANNELS; ch++)
        {
            for (short i=0; i<3; i++) fs >> s;

            samples = ch_entries[ch].samples;
            peak = ch_entries[ch].peak; 
            zl = &ch_entries[ch].zl; 
            charge = &ch_entries[ch].charge;
            
            *zl = 0;
            min = numeric_limits<short>::max();

            for (short si=0; si<N_SAMPLES; si++)
            {
                fs >> s;
                samples[si] = stoi(s);
                if (samples[si] < min)
                {
                    min = samples[si];
                    peak[1] = si;
                }
                *zl += samples[si];
            }
            peak[0] = peak[2] = peak[1];
            *zl -= min;
            short width=1;
            while (peak[0]>0 && samples[peak[0]-1]>=samples[peak[0]])
            {
                width++;
                *zl -= samples[--peak[0]];
            }
            while (peak[2]<N_SAMPLES-1 && samples[peak[2]]<=samples[peak[2]+1])
            {
                width++;
                *zl -= samples[++peak[2]];
            }
            
            width = N_SAMPLES - width;
            *zl = (width) ? *zl/width : min-1;
            ch_entries[ch].amp = *zl - min;

            // Подсчитываем заряд в определённом интервале
            *charge = 0;
            if (width) for (short si=peak[0]; si<=peak[2]; si++)
				*charge += *zl - samples[si];
        }
        adc64->Fill();
    }

    adc64->Write();
    hodo.Close();
}

int main(int argc, char**argv)
{
    cout << "Processing file: " << argv[1] << "...\n";
    conv(argv[1], argv[2]);
    return 0;
}
